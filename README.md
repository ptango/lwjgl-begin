# Premiers pas avec LWJGL

(Attention aux yeux si vous regardez le code.)


## Ce que je veux faire
A terme, ce qui ressemble à un logiciel CAO pour la mécanique. Pour l'instant, j'ai juste travaillé (en 2D) sur l'assemblage de carrés de différentes tailles.

## Ce qui marche à peu près
On peut générer des carrés de différentes tailles (touches 'r' 't' 'y'). On peut sélectionner les arrêtes de 2 carrés différents et leur appliquer une contrainte : alignement (touche 'a'), parallélisme (touche 'p') et collage (touche 'c'). Pour réinitialiser la sélection : 's'.


## Comment faire fonctionner ?
On peut utiliser le makefile : `make run` pour compiler et exécuter.
