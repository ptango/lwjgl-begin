
public abstract class Element {


    protected int x;
    protected int y;


    public int getX(){
        return this.x;
    }


    public int getY(){
        return this.y;
    }


    public void setX(int x){
        this.x = x;
    }


    public void setY(int y){
        this.y = y;
    }

    public void move(int x, int y){
        this.setX(x);
        this.setY(y);
    }


    public boolean matchs(int m, int p){
        return false;
    }


    public abstract void draw();
    public void draw(boolean isSelected){

    }

}