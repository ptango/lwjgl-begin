

class Contrainte {

    // Les 2 pièces contraintes
    private Rectangle piece1;
    private Rectangle piece2;


    // Degrés de liberté de la pièce 1
    private boolean p1_tx = false;
    private boolean p1_ty = false;


    // Degrés de liberté de la pièce 2
    private boolean p2_tx = false;
    private boolean p2_ty = false;

    public Contrainte(Rectangle p1, Rectangle p2){
        this.piece1 = p1;
        this.piece2 = p2;
        System.out.println("Ajout d'une contrainte");
    }


    public boolean getP1TX(){
        return this.p1_tx;
    }

    public void setP1TX(boolean o){
        this.p1_tx = o;
    }

    public boolean getP1TY(){
        return this.p1_ty;
    }

    public void setP1TY(boolean o){
        this.p1_ty = o;
    }

    public boolean getP2TX(){
        return this.p2_tx;
    }

    public void setP2TX(boolean o){
        this.p2_tx = o;
    }

    public boolean getP2TY(){
        return this.p2_ty;
    }

    public void setP2TY(boolean o){
        this.p2_ty = o;
    }

}