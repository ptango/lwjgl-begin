import org.lwjgl.*;
import org.lwjgl.input.*;
import org.lwjgl.opengl.*;

import java.util.*;

public class App {

    private Rectangle sel1 = null;
    private Rectangle sel2 = null;

    private boolean selectionActive = false;

    private void resetSelection(){
        sel1 = null;
        sel2 = null;
        selectionActive = false;
        System.out.println("Selection canceled");
    }

    public void start() {
        try {
            Display.setDisplayMode(new DisplayMode(800,600));
            Display.create();
        } catch (LWJGLException e) {
            e.printStackTrace();
            System.exit(0);
        }

        // init OpenGL here
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glLoadIdentity();
        GL11.glOrtho(0, 800, 0, 600, 1, -1);
        GL11.glMatrixMode(GL11.GL_MODELVIEW);

        Rectangle r;
        Rectangle base = new Rectangle(300,300, 200);
        base.libX(false);
        base.libY(false);

        LinkedList<Element> list = new LinkedList<Element>();
        list.add(base);



        Element current = null;

        int ox = -1;
        int oy = -1;
        boolean enDeplacement = false;

        boolean selectionActive = false;
        boolean libX = true;

        while (!Display.isCloseRequested()) {



            // Events :
            int x = Mouse.getX();
            int y = Mouse.getY();
            // System.out.println(x + " " + y);


            while (Keyboard.next()) {
                if (Keyboard.getEventKeyState()) {
                    // Down

                    // Ajouter des rectangles
                    if (Keyboard.getEventKey() == Keyboard.KEY_R) {
                        list.add(new Rectangle(x - 100,y - 100, 200));
                    }
                    if (Keyboard.getEventKey() == Keyboard.KEY_T) {
                        list.add(new Rectangle(x - 75,y - 75, 150));
                    }
                    if (Keyboard.getEventKey() == Keyboard.KEY_Y) {
                        list.add(new Rectangle(x - 50,y - 50, 100));
                    }

                    if (Keyboard.getEventKey() == Keyboard.KEY_P) {
                        list.add(new Point(x,y));
                    }

                    if (Keyboard.getEventKey() == Keyboard.KEY_Q) {
                        Point p1 = new Point(x,y);
                        Point p2 = new Point(x+50,y+50);
                        Point p3 = new Point(x+25,y+25);
                        list.add(p1);
                        list.add(p2);
                        list.add(p3);
                        Segment s = new Segment(p1,p2,p3);
                        list.add(s);
                        p3.setContainte(s);
                    }

                    // Ajouter une contrainte
                    if (Keyboard.getEventKey() == Keyboard.KEY_C) {
                        selectionActive = true;
                        System.out.println("Activation de la sélection");
                    }

                    if (Keyboard.getEventKey() == Keyboard.KEY_RETURN) {
                        if (selectionActive && sel1 != null && sel2 != null) {
                            sel1.sAligner(sel2);
                        }
                    }


                    // Reset sélection
                    if (Keyboard.getEventKey() == Keyboard.KEY_S) {
                        resetSelection();
                    }
                } else {
                    // UP
                }
            }

            if (Mouse.isButtonDown(0)) {

                for (Element e : list) {
                    if (e.matchs(x,y)) {
                        if (!enDeplacement) {
                            current = e;
                        }
                        if (e instanceof Rectangle) {
                            Rectangle re = (Rectangle)e;
                            if (selectionActive) {
                                if (sel1 == null) {
                                    sel1 = re;
                                    sel1.setSelectedArea(x,y);
                                    System.out.println("sel1");
                                }else{
                                    if (sel2 == null && sel1 != re) {
                                        sel2 = re;
                                        sel2.setSelectedArea(x,y);
                                        System.out.println("sel2");
                                    }
                                }
                            }
                        }
                    }
                }
                if (current != base) {
                    enDeplacement = true;
                    if (current instanceof Point) {
                        // current.setX(x);
                        // current.setY(y);
                        current.move(x,y);
                    }else{
                        int rx = current.getX();
                        int ry = current.getY();

                        if (ox == -1 || oy == -1) {
                            ox = x - rx;
                            oy = y - ry;
                        }

                        current.move(x - ox,y - oy);
                    }
                }

            }else{
                ox = -1;
                oy = -1;
                enDeplacement = false;
            }


            // Clear the screen and depth buffer
            GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);

            for (Element e : list) {
                e.draw();
                if (current != null) {
                    if (e == current) {
                        e.draw(true);
                    }
                }
            }


            Display.update();
        }

        Display.destroy();
    }


    public static void main(String[] argv) {
        App displayExample = new App();
        displayExample.start();
    }
}