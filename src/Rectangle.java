import org.lwjgl.*;
import org.lwjgl.input.*;
import org.lwjgl.opengl.*;

import java.util.*;

class Rectangle extends Element {

    private int cote = 200;

    private int offset = cote / 10;

    private boolean tx = true;
    private boolean ty = true;

    private char selectedArea = 'm';

    private LinkedList<Rectangle> rectListContrains = new LinkedList<Rectangle>();

    private LinkedList<Contrainte> contraintes = new LinkedList<Contrainte>();



    public Rectangle(int x, int y, int cote){
        this.x = x;
        this.y = y;
        this.cote = cote;
    }

    public void draw(){
        this.draw(false);
    }

    public void draw(boolean isSelected){
        // set the color of the quad (R,G,B,A)
        if (isSelected) {
            GL11.glColor3f(0.5f,0.5f,0f);
        }else{
            GL11.glColor3f(0.5f,0.5f,1.0f);
        }


        // draw quad
        GL11.glBegin(GL11.GL_QUADS);
            GL11.glVertex2f(x,y);
            GL11.glVertex2f(x+this.cote,y);
            GL11.glVertex2f(x+this.cote,y+this.cote);
            GL11.glVertex2f(x,y+this.cote);
        GL11.glEnd();






        GL11.glColor3f(0f,0.5f,1.0f);
        GL11.glBegin(GL11.GL_QUADS);
            GL11.glVertex2f(x,y);
            GL11.glVertex2f(x,y+this.cote);
            GL11.glVertex2f(x + offset,y+this.cote - offset);
            GL11.glVertex2f(x + offset, y + offset);
        GL11.glEnd();


        GL11.glColor3f(0.2f,0.6f,1.0f);
        GL11.glBegin(GL11.GL_QUADS);
            GL11.glVertex2f(x,y);
            GL11.glVertex2f(x+this.cote,y);
            GL11.glVertex2f(x+this.cote - offset,y + offset);
            GL11.glVertex2f(x + offset, y + offset);
        GL11.glEnd();

        GL11.glColor3f(0.4f,0.2f,1.0f);
        GL11.glBegin(GL11.GL_QUADS);
            GL11.glVertex2f(x+this.cote,y);
            GL11.glVertex2f(x+this.cote,y+this.cote);
            GL11.glVertex2f(x+this.cote - offset,y+this.cote - offset);
            GL11.glVertex2f(x+this.cote - offset,y + offset);
        GL11.glEnd();


        GL11.glColor3f(0.6f,0.8f,1.0f);
        GL11.glBegin(GL11.GL_QUADS);
            GL11.glVertex2f(x+this.cote,y+this.cote);
            GL11.glVertex2f(x,y+this.cote);
            GL11.glVertex2f(x + offset,y+this.cote - offset);
            GL11.glVertex2f(x+this.cote - offset,y+this.cote - offset);
        GL11.glEnd();
    }





    public boolean matchs(int m, int p){
        return (m > this.x && m < this.x + this.cote) &&
                (p > this.y && p < this.y + this.cote);
    }


    public void move(int x, int y){
        if (tx) {
            this.x = x;
        }
        if (ty) {
            this.y = y;
        }
    }





    public void setX(int x){
        if (tx) {
            this.x = x;
        }else{
            System.out.println("Impossible de bouger X. Pas assez de degré de liberté !");
        }
    }


    public void setY(int y){
        if (ty) {
            this.y = y;
        }else{
            System.out.println("Impossible de bouger Y. Pas assez de degré de liberté !");
        }
    }




    public int getCote(){
        return this.cote;
    }

    public void libX(boolean lib){
        this.tx = lib;
    }

    public void libY(boolean lib){
        this.ty = lib;
    }


    public void setSelectedArea(int sx, int sy){
        if (matchs(sx,sy)) {
            if ((sx > this.x && sx < this.x + offset) &&
                (sy > this.y + offset && sy < this.y + cote - offset)) {
                this.selectedArea = 'l';
                System.out.println("Coté l");
            }else if ((sx > this.x + cote - offset && sx < this.x + cote) &&
                (sy > this.y + offset && sy < this.y + cote - offset)) {
                this.selectedArea = 'r';
                System.out.println("Coté r");
            }else if ((sx > this.x + offset && sx < this.x + cote - offset) &&
                (sy > this.y && sy < this.y + offset)) {
                this.selectedArea = 'b';
                System.out.println("Coté b");
            }else if ((sx > this.x + offset && sx < this.x + cote - offset) &&
                (sy > this.y + cote - offset && sy < this.y + cote)) {
                this.selectedArea = 't';
                System.out.println("Coté t");
            }else{
                this.selectedArea = 'm';
                System.out.println("Coté m");
            }
        }
    }


    public char getSelectedArea(){
        return this.selectedArea;
    }


    public boolean correctValueSelectedArea(){
        boolean correct = false;
        if (this.selectedArea == 'l' ||
            this.selectedArea == 'r' ||
            this.selectedArea == 't' ||
            this.selectedArea == 'b') {
            correct = true;
        }
        return correct;
    }




    public boolean estParallele(Rectangle r){
        boolean correct = false;
        char sar = r.getSelectedArea();
        if(this.correctValueSelectedArea() && r.correctValueSelectedArea()){
            if (((this.selectedArea == 'l' || this.selectedArea == 'r') &&
                (sar == 'l' || sar == 'r')) ||
                ((this.selectedArea == 't' || this.selectedArea == 'b') &&
                (sar == 't' || sar == 'b'))) {
                correct = true;
            }
        }
        return correct;
    }


    public void sAligner(Rectangle r){
        this.addRectContaint(r);
        char sar = r.getSelectedArea();
        char sat = this.selectedArea;
        if (this.estParallele(r)) {
            if (sar == 'l' && sat == 'l') {
                this.setX(r.getX());
                this.libX(false);
                r.libX(false);
            }else if(sar == 'l' && sat == 'r'){
                this.setX(r.getX() - this.cote);
                this.libX(false);
                r.libX(false);
            }else if(sar == 'r' && sat == 'l'){
                this.setX(r.getX() + r.getCote());
                this.libX(false);
                r.libX(false);
            }else if(sar == 'r' && sat == 'r'){
                this.setX(r.getX() + r.getCote() - this.getCote());
                this.libX(false);
                r.libX(false);
            }else if(sar == 't' && sat == 't'){
                this.setY(r.getY() + r.getCote() - this.getCote());
                this.libY(false);
                r.libY(false);
            }else if(sar == 't' && sat == 'b'){
                this.setY(r.getY() + r.getCote());
                this.libY(false);
                r.libY(false);
            }else if(sar == 'b' && sat == 't'){
                this.setY(r.getY() - this.getCote());
                this.libY(false);
                r.libY(false);
            }else if(sar == 'b' && sat == 'b'){
                this.setY(r.getY());
                this.libY(false);
                r.libY(false);
            }
        }
    }


    public void seParalleliser(Rectangle r){
        this.addRectContaint(r);
        char selA1 = this.getSelectedArea();
        char selA2 = r.getSelectedArea();
        if ((selA1 == 'l' || selA1 == 'r') &&
            (selA2 == 'l' || selA2 == 'r')) {
            this.libX(false);
            r.libX(false);
            System.out.println("Ajout de la contrainte de parallélisme");
        }else if ((selA1 == 't' || selA1 == 'b') &&
            (selA2 == 't' || selA2 == 'b')) {
            this.libY(false);
            r.libY(false);
            System.out.println("Ajout de la contrainte de parallélisme");
        }else{
            System.out.println("Incompatibilité pour le parallélisme");
        }
    }


    public void addRectListContrains(Rectangle r){
        this.rectListContrains.add(r);
    }


    public void addRectContaint(Rectangle r){
        Contrainte c = new Contrainte(this,r);
        r.addRectListContrains(this);
        this.rectListContrains.add(r);
        this.contraintes.add(c);
    }


}