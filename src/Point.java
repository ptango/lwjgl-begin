import org.lwjgl.*;
import org.lwjgl.input.*;
import org.lwjgl.opengl.*;

import java.util.*;

class Point extends Element {


    private Element containte;

    public Point(int x, int y){
        this.x = x;
        this.y = y;
    }

    public void draw(){
        this.draw(false);
    }

    public void draw(boolean isSelected){
        // set the color of the quad (R,G,B,A)


        // draw quad
        GL11.glBegin(GL11.GL_POINTS);
            GL11.glVertex2f(x, y);
        GL11.glEnd();


        GL11.glBegin(GL11.GL_LINES);
            GL11.glVertex2f(x - 10, y - 10);
            GL11.glVertex2f(x + 10, y + 10);
        GL11.glEnd();

        GL11.glBegin(GL11.GL_LINES);
            GL11.glVertex2f(x + 10, y - 10);
            GL11.glVertex2f(x - 10, y + 10);
        GL11.glEnd();
    }

    public boolean matchs(int m, int p){
        int offset = 10;
        return (m > x - offset && m < x + offset) && (p > y - offset && p < y + offset);
    }



    public void setContainte(Element e){
        containte = e;
    }


    public void move(int x, int y){
        if (containte != null) {
            y = f(x);
            // System.out.println(y);
            this.setX(x);
            this.setY(y);
        }else{
            this.setX(x);
            this.setY(y);
        }
    }


    public void setX(int x){
        this.x = x;
    }


    public void setY(int y){
        this.y = y;
    }


    public int f(int xx){
        Segment s = (Segment)containte;
        double angle = s.getAngle();
        int mxx = (int)(angle * (double)xx);
        int x = s.getPoint1().getX();
        int y = s.getPoint1().getY();
        int b = y - (int)(angle * (double)x);
        return mxx + b;
    }


}