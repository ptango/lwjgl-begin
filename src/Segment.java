import org.lwjgl.*;
import org.lwjgl.input.*;
import org.lwjgl.opengl.*;

import java.util.*;

class Segment extends Element {

    private Point p1;
    private Point p2;
    private Point p3;

    public Segment(Point p1, Point p2){
        this.p1 = p1;
        this.p2 = p2;
    }

    public Segment(Point p1, Point p2, Point p3){
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
    }


    public void draw(){
        // set the color of the quad (R,G,B,A)


        // draw quad
        p1.draw();
        p2.draw();


        GL11.glBegin(GL11.GL_LINES);
            GL11.glVertex2f(p1.getX(), p1.getY());
            GL11.glVertex2f(p2.getX(), p2.getY());
        GL11.glEnd();
        getAngle();
    }


    public double getAngle(){
        double x = p2.getX() - p1.getX();
        double y = p2.getY() - p1.getY();
        return y / x;
    }


    public Point getPoint1(){
        return p1;
    }



}