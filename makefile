SRC = src
#SOURCES = $(shell find -name *.java)
SOURCES = src/*.java
S = java
SC = javac
TARGET = bin
CP = bin:.:lib/jinput.jar:lib/lwjgl-debug.jar:lib/lwjgl.jar:lib/lwjgl_test.jar:lib/lwjgl_util_applet.jar:lib/lwjgl_util.jar

compile: $(SOURCES:.java=.class)

%.class: %.java
	clear
	@echo ":: Compiling..."
	@echo "Compiling $*.java.."
	@$(SC) -cp $(CP) -d $(TARGET) $*.java

run: compile
	@echo ":: Executing..."
	@$(S) -cp $(CP) -Djava.library.path=native/linux App


clean:
	@$(RM) $(SRC)/*/*.class




#javac -d bin -cp bin:.:lib/jinput.jar:lib/lwjgl-debug.jar:lib/lwjgl.jar:lib/lwjgl_test.jar:lib/lwjgl_util_applet.jar:lib/lwjgl_util.jar src/App.java

#java -cp bin:.:lib/jinput.jar:lib/lwjgl-debug.jar:lib/lwjgl.jar:lib/lwjgl_test.jar:lib/lwjgl_util_applet.jar:lib/lwjgl_util.jar -Djava.library.path=native/linux App